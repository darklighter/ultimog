# UltiMoG - The grand high Ultimate Movie-goer's Guide!

UltiMoG is a piece of demonstration software, demo-ing both [React][lnk_react] and [The Movie DataBase API][lnk_tmdb].

There is an online instance of this at [ultimog.ginomai.com][lnk_umg].


[lnk_react]: http://reactjs.org
[lnk_tmdb]: https://www.themoviedb.org
[lnk_umg]: https://ultimog.ginomai.com