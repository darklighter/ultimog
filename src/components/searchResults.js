import React, { Component } from "react";

class SearchResults extends Component {

  constructor( props ) {
    super( props )

    this.state = {
      term : "Metropolis"
    }
  }

  render() {
    return (
      <div class="tab">
        <input id="tab-one" type="checkbox" name="tabs"/>
        <label for="tab-one">{}</label>
        <div class="tab-content">
          <ul id="searchResults">
            <li>Metropolis</li>
            <li>movie 2</li>
            <li>movie 3</li>
            <li>movie 4</li>
            <li>movie 5</li>
            <li>movie 6</li>
            <li>movie 7</li>
          </ul>
        </div>
      </div>
    );
  }

}

module.exports = Search;