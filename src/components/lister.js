import React, { Component } from "react";


/*                                                    component definitions  */

/**
 *  Component to build an <li> element.
 */
class ListItem extends Component {

  constructor ( props ) {
    super( props );

    this.handleClick = this.handleClick.bind( this );

    this.getMovie = this.props.getMovie;

    this.mid = 0;

  }

  handleClick ( event ) {
    const
      { mid } = event.target;

      this.getMovie( this.mid );
  }

  render() {

    this.mid = this.props.mid;

    return (<li>
      <button
        mid={this.props.mid}
        onClick={this.handleClick}
        title={this.props.over}
      >{this.props.ttl}</button>
    </li>);
  }

}

/**
 *  Builds a <ul> list element
 */
class Lister extends Component {

  constructor ( props ) {
    super( props );

    this.state = {
      label : this.props.label,
      list  : this.props.list
    }

    this.props.execOnce && this.props.execOnce();
  }

  render() {

    /*
     *  Create a random string for tab id's
     */
    function getRandomString() {
      const alpha = ['a','b','c','d','e','f','g','h','i','j'];
      let ret = '';

      for (let i=0; i<10; i++) {
        ret += alpha[Math.floor(Math.random() * Math.floor(10))];
      }

      return ret;
    }

    const get = this.props.getMovie;

    let tId = getRandomString();

    return (
      <div className="tab">
        <input id={tId} type="checkbox" name="tabs"/>
        <label htmlFor={tId}>{this.props.label}</label>
        <div className="tab-content">
          <ul className="list">
            {this.props.list.map( ( mov ) => (
                 <ListItem ttl={mov.title} mid={mov.id} over={mov.overview} getMovie={get} />
            ))}
          </ul>
        </div>
      </div>
    );
  }

}

export default Lister;