import React, { Component } from "react";

const _buffer = 2000;  //  input event throttle

let _lastInput, _TO;

let __this;


/*                                                     component definition  */

/**
 *  Builds an <input> element. Though this component seems simple,
 *  it can be the basis for a more exotic search component that
 *  could do dynamic lookaheads, etc.
 */
class SearchTerm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      term : ''
    };
  }

  handleChange ( event ) {
    const
      currentInput = Date.now(),
      { value } = event.target;

    clearTimeout(_TO);

    if ( (currentInput - _lastInput) > _buffer ) {
      __this.setState({ 'term' : value });
      __this.props.getResults( value );
    }
    else {

      _TO = setTimeout( () => {
          __this.setState({ 'term' : value });
          __this.props.getResults( value );
        }, _buffer);

    }

    _lastInput = currentInput;
  }

  render() {
    __this = this;

    return (
         <input
           id="searchterm"
           onKeyUp={this.handleChange}
           placeholder="search me"
           type="text"
         />
    );
  }

}

export default SearchTerm;