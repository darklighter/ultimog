import React, { Component } from "react";

const
  _rootAPI = 'https://www.youtube-nocookie.com/embed/';

/**
 *  YouTube mini-viewer component.
 *
 *  props :
 *    backclick : function to run when backdrop is clicked
 *    id : youtube id of video to show
 */
class YouView extends Component {

  render() {
    let src = _rootAPI + this.props.id;

    return (
      <div className="backdrop flx" onClick={ this.props.backclick }>
        <iframe
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
          frameBorder="0"
          height="315"
          src={ src }
          width="560"
          >
        </iframe>
      </div>
    );
  }

}

export default YouView;