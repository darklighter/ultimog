import React, { Component } from "react";

import Lister from "./lister";
import YouView from "./youView";


/*                                                        utility functions  */

/**
 *  Reduce an array of objects to a string using the 'name' field.
 *
 *  @param  ary
 *
 *  @returns  string
 */
function pluckName( ary ) {
  let tAry = [];

  ary && ary.forEach( item => { tAry.push(item.name); });

  return tAry.join(', ');
};

let img_backdrop;
let _Movie;

/*                                                     component definition  */

/**
 *  Movie details display component.
 */
class Movie extends Component {
  componentDidUpdate () {
    //  just for fun
    document.body.style.backgroundImage = 'url(' + img_backdrop + ')';
  }

  constructor() {
    super();

    this.state = {
      previewId : ''
    }

  }

  /**
   *  Requests movie data by movie ID.
   *
   *  @param  id  {integer}  Number of movie to get data for.
   */
  showPreview ( id ) {

    _Movie.setState({
        previewId : id || ''
    });

  }

  render() {
    const
      data = this.props.data,
      img  = {
        back   : 'https://image.tmdb.org/t/p/original' + data.backdrop,
        poster : 'https://image.tmdb.org/t/p/w500' + data.poster
      },
      genres = pluckName( data.genre ),
      langs = pluckName( data.spoken_languages ),
      prodCmp = pluckName( data.production_companies ),
      prodCou = pluckName( data.production_countries );

    img_backdrop = img.back;

    data.vote = (data.vote && (data.vote > -1)) ? data.vote : '-';

    _Movie = this;

    return (
      <div className="abstract">
        <img className="movieThumb" src={ img.poster }/>
        <div className="fs3 movieTitle" title={ data.original_title }>{ data.title }</div>
        <span className="tagline">{ data.tagline }</span>
        <div className="overview">{ data.overview }</div><br/>
        <div className="links flx w100">
          { (data.videos && data.videos.Teaser) && <Lister label="Teaser" list={ data.videos.Teaser } getMovie={ this.showPreview } /> }
          { (data.videos && data.videos.Trailer) && <Lister label="Trailer" list={ data.videos.Trailer } getMovie={ this.showPreview } /> }
        </div><br/>
        <div className="flx w100"><div>{ genres }</div><div>{ prodCmp }</div></div><br/>
        <div className="flx w100"><div>Original Release</div><div>{ data.release }</div></div>
        <div className="flx w100"><div>Running Time</div><div>{ data.runtime } mins</div></div>
        <div className="flx w100"><div>Box Office</div><div>${ data.revenue }</div></div>
        <div className="flx w100"><div>Vote Average</div><div>{ data.vote } / 10</div></div>
        {
          this.state.previewId.length && <YouView id={ this.state.previewId } backclick={ this.showPreview } />
        }
      </div>
    );
  }

}

export default Movie;