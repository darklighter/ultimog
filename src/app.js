import React, { Component} from 'react';

import { hot } from "react-hot-loader";

import Movie from "./components/movie";
import SearchTerm from "./components/searchTerm";
import Lister from "./components/lister";

import "./all.css";

const
  _apiVals = {
    apikey : 'api_key=7c84ae500a6f382d38257b7e2a21e690',
    lang  : 'language=en-US',
    page : 'page=1',
    rootAPI : 'https://api.themoviedb.org/3/'
  };

let
  __app;

class App extends Component {

  componentDidMount() {

    this.getMovie( this.state.id );

  }

  constructor ( props ) {
    super( props )

    this.state = {
      id : 19, // Metropolis
      nowPlaying : [],
      popular : [],
      searchResults : [],
      searchTerm : 'search results',
      topRated : []
    }
  }

  /**
   *  Requests movie data by movie ID.
   *
   *  @param  id  {integer}  Number of movie to get data for.
   */
  getMovie ( id ) {

    fetch( `${_apiVals.rootAPI}movie/${id}?&${_apiVals.apikey}` )
      .then( ( res ) => res.json() )
      .then( ( data ) => {
          // update state with API data
          __app.setState({
              backdrop : data.backdrop_path,     //  "/xxx.(..g)"
              budget	 : data.budget,            //  int
              genre    : data.genres,            //  [{id,name},]
              homepage : data.homepage,          //  url
              id       : data.id,                //  int
              imdb_id	 : data.imdb_id,           //  string
              original_language : data.original_language,  //  "en"
              original_title    : data.original_title,     // string
              overview : data.overview,          //  string
              popularity : data.popularity,      //  float
              poster   : data.poster_path,       //  "/xxx.(..g)"
              production_companies : data.production_companies,  //  [{id,logo_path,name,origin_country},]
              production_countries : data.production_countries,  //  [{iso_3166_1,name},]
              release  : data.release_date,      //  "yyyy-mm-dd"
              revenue  : data.revenue,           //  int
              runtime  : data.runtime,           //  int (minutes)
              spoken_languages : data.spoken_languages,    //  [{iso_639_1,name},]
              tagline  : data.tagline,           //  string
              title    : data.title,             //  string
              vote_average : data.vote_average,  //  float
              vote_count   : data.vote_count     //  int
          });

      });

    fetch( `${_apiVals.rootAPI}movie/${id}/videos?${_apiVals.apikey}&${_apiVals.lang}` )
      .then( ( res ) => res.json() )
      .then( ( data ) => {
          const vids = {
            Teaser : [],
            Trailer : []
          };

          data.results.forEach( ( res ) => {
              vids[ res.type ].push({
                  id : res.key,
                  overview : res.name,
                  title :  res.name
              })
          })

          __app.setState({
              videos : vids
          });
      });

  }

  /**
   *  Requests movie data by movie ID.
   *
   *  @param  id  {integer}  Number of movie to get data for.
   */
  getSearchResults ( term ) {

    __app.setState({
        searchTerm : term
    });

    fetch( `${_apiVals.rootAPI}search/movie?${_apiVals.apikey}&${_apiVals.lang}&${_apiVals.page}&include_adult=false&query=${term}` )
      .then( ( res ) => res.json() )
      .then( ( data ) => {
          __app.setState({
              searchResults : data.results
          });
      });

  }

  /**
   *  Requests movie data for films now playing.
   */
  getNowPlaying () {

    fetch( `${_apiVals.rootAPI}movie/now_playing?${_apiVals.apikey}&${_apiVals.lang}&${_apiVals.page}` )
      .then( ( res ) => res.json() )
      .then( ( data ) => {
          __app.setState({
              nowPlaying : data.results
          });
      });

    return this;
  }

  /**
   *  Requests movie data for most popular films.
   */
  getPopular () {

    fetch( `${_apiVals.rootAPI}movie/popular?${_apiVals.apikey}&${_apiVals.lang}&${_apiVals.page}` )
      .then( ( res ) => res.json() )
      .then( ( data ) => {
          __app.setState({
              popular : data.results
          });
      });

    return this;
  }

  /**
   *  Requests movie data for top rated films.
   */
  getTopRated () {

    fetch( `${_apiVals.rootAPI}movie/top_rated?${_apiVals.apikey}&${_apiVals.lang}&${_apiVals.page}` )
      .then( ( res ) => res.json() )
      .then( ( data ) => {
          __app.setState({
              topRated : data.results
          });
      });

    return this;
  }


  render() {
    __app = this;

    return (
      <div>
        <header>
          <div id="header" className="flx">
            <div className="appName fs4 left"><span className="r45l">U</span>lti<span className="r45l">M</span>o<span className="r45l">G</span></div>
            <div className="searchInpCont flxi cntr">
              <SearchTerm getResults={this.getSearchResults}/>
            </div>
          </div>
        </header>
        <br/>
        <div className="top flx w100">
          <div className="colLeft lists">
            <Lister label={this.state.searchTerm} list={this.state.searchResults} getMovie={this.getMovie} />
            <Lister label="Now Playing" list={this.state.nowPlaying} getMovie={this.getMovie} execOnce={this.getNowPlaying} />
            <Lister label="Popular" list={this.state.popular} getMovie={this.getMovie} execOnce={this.getPopular} />
            <Lister label="Top Rated" list={this.state.topRated} getMovie={this.getMovie} execOnce={this.getTopRated} />
          </div>
          <div className="colRight">
            <Movie data={this.state} />
          </div>
        </div>
      </div>
    );
  }
}

export default hot(module)(App);