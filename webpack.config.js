const path = require("path");
const webpack = require("webpack");

module.exports = {
  entry : "./src/index.js",
  module : {
    rules : [{
        test : /\.(js|jsx)$/,
        exclude : /(node_modules|bower_components)/,
        loader : "babel-loader",
        options : {
          // This is a feature of `babel-loader` for webpack (not Babel itself).
          // It enables caching results in ./node_modules/.cache/babel-loader/
          // directory for faster rebuilds.
//          cacheDirectory: true,
//          plugins: ['react-hot-loader/babel'],
          presets: ["@babel/env"]
        }
      }, {
        test : /\.css$/,
        use : ["style-loader", "css-loader"]
      }
    ]
  },
  resolve : {
    extensions : ["*", ".js", ".jsx"]
  },
  output : {
    path : path.resolve(__dirname, "dist/"),
    publicPath : "/dist/",
    filename : "bundle.js"
  },
  devServer : {
    contentBase : path.join(__dirname, "public/"),
    port : 3000,
    publicPath : "http://localhost:3000/dist/",
    hotOnly : true
  },
  plugins : [new webpack.HotModuleReplacementPlugin()]
};
